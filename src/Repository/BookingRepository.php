<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\Tenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }


    public function findByTenantId(Tenant $tenant) {
        try {
            return $this->createQueryBuilder('b')
                ->where('b.tenant = :tenant')
                ->setParameter('tenant', $tenant->getId())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getBookingsByBookingObjectId(int $bookingObjectId)
    {
        return $this->createQueryBuilder('b')
            ->select('b.room')
            ->addSelect('b.expiration_date')
            ->where('b.bookingObject = :bookingObject')
            ->setParameter('bookingObject', $bookingObjectId)
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
}
