<?php

namespace App\Repository;

use App\Entity\Pension;

/**
 * @method Pension|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pension|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pension[]    findAll()
 * @method Pension[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PensionRepository extends BookingObjectRepository
{
}
