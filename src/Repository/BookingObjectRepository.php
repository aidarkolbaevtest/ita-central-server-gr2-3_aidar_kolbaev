<?php

namespace App\Repository;

use App\Entity\BookingObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BookingObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingObject[]    findAll()
 * @method BookingObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingObjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingObject::class);
    }

    public function findOneByNameOrContactNumber(string $name, string $contact_number) : ?BookingObject
    {
        try {
            return $this->createQueryBuilder('b')
                ->select('b')
                ->where('b.name = :name')
                ->orWhere('b.contact_number = :contact_number')
                ->setParameter('name', $name)
                ->setParameter('contact_number', $contact_number)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    public function findByParameters(string $name, string $type, string $min_price, string $max_price) {
        $query = $this->createQueryBuilder('b')->select('b');
        if (!empty($name)) {
            $query->where('b.name LIKE :name')->setParameter('name', '%'.$name.'%');
        }
        if (!empty($type)) {
            $query->andWhere('b.type = :type')->setParameter('type', $type);
        }
        return $query->andWhere('b.price_per_night BETWEEN :min_price AND :max_price')
                    ->setParameter('min_price', $min_price ?: 100)
                    ->setParameter('max_price', $max_price ?: 10000)
                    ->getQuery()->getResult();
    }

}
