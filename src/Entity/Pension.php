<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PensionRepository")
 */
class Pension extends BookingObject
{
    /**
     * @ORM\Column(type="boolean")
     */
    private $beach;


    /**
     * @ORM\Column(type="float")
     */
    private $distance_to_coast;

    /**
     * @param mixed $beach
     * @return Pension
     */
    public function setBeach(bool $beach)
    {
        $this->beach = $beach;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBeach()
    {
        return $this->beach;
    }

    /**
     * @param mixed $distance_to_coast
     * @return Pension
     */
    public function setDistanceToCoast(float $distance_to_coast)
    {
        $this->distance_to_coast = $distance_to_coast;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistanceToCoast()
    {
        return $this->distance_to_coast;
    }


    public function __toArray() {
        return array_merge(parent::__toArray(), [
            'beach' => $this->beach,
            'distance_to_coast' => $this->distance_to_coast
        ]);
    }

}
