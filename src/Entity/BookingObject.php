<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingObjectRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"booking_object" = "BookingObject", "pension" = "Pension", "cottage" = "Cottage"})
 */
class BookingObject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=512, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $number_of_rooms;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Regex(
     *     pattern="#^0[357]\d\d{7}$#",
     *     message="Неправильный номер телефона"
     * )
     */
    private $contact_number;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $address;

    /**
     * @ORM\Column(type="float")
     */
    private $price_per_night;

    /**
     * @var Landlord
     * @ORM\ManyToOne(targetEntity="App\Entity\Landlord", inversedBy="booking_objects")
     */
    private $landlord;


    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="tenant")
     */
    private $bookings;


    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;


    public function __construct()
    {
        $this->bookings = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * @param Booking $booking
     * @return $this
     */
    public function addBooking(Booking $booking) {
        $this->bookings->add($booking);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNumberOfRooms(): ?int
    {
        return $this->number_of_rooms;
    }

    public function setNumberOfRooms(int $number_of_rooms): self
    {
        $this->number_of_rooms = $number_of_rooms;

        return $this;
    }

    public function getContactNumber(): ?string
    {
        return $this->contact_number;
    }

    public function setContactNumber(string $contact_number): self
    {
        $this->contact_number = $contact_number;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPricePerNight(): ?float
    {
        return $this->price_per_night;
    }

    public function setPricePerNight(float $price_per_night): self
    {
        $this->price_per_night = $price_per_night;

        return $this;
    }

    /**
     * @param Landlord $landlord
     * @return BookingObject
     */
    public function setLandlord(Landlord $landlord): ?BookingObject
    {
        $this->landlord = $landlord;
        return $this;
    }

    /**
     * @return Landlord
     */
    public function getLandlord(): ?Landlord
    {
        return $this->landlord;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return BookingObject
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }


//    private function getBusyRooms() {
//    }

    public function __toArray() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'address' => $this->address,
            'contact_number' => $this->contact_number,
            'landlord' => $this->landlord->getId(),
            'number_of_rooms' => $this->number_of_rooms,
            'price' => $this->price_per_night,
            'type' => $this->type
        ];
    }
}
