<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TenantRepository")
 */
class Tenant extends Client
{
    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="tenant")
     */
    private $bookings;


    public function __construct()
    {
        parent::__construct();
        $this->bookings = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * @param Booking $booking
     * @return $this
     */
    public function addBooking(Booking $booking) {
        $this->bookings->add($booking);
        return $this;
    }
}
