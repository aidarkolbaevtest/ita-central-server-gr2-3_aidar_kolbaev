<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LandlordRepository")
 */
class Landlord extends Client
{
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BookingObject", mappedBy="landlord")
     */
    private $booking_objects;


    public function __construct()
    {
        parent::__construct();
        $this->booking_objects = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getBookingObjects()
    {
        return $this->booking_objects;
    }

    public function addBookingObject(BookingObject $bookingObject) {
        $this->booking_objects->add($bookingObject);
        return $this;
    }
}
