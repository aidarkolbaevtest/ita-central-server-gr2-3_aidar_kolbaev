<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CottageRepository")
 */
class Cottage extends BookingObject
{
    /**
     * @ORM\Column(type="boolean")
     */
    private $garage;


    /**
     * @ORM\Column(type="boolean")
     */
    private $pool;

    /**
     * @param mixed $garage
     * @return Cottage
     */
    public function setGarage(bool $garage)
    {
        $this->garage = $garage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGarage()
    {
        return $this->garage;
    }

    /**
     * @param mixed $pool
     * @return Cottage
     */
    public function setPool(bool $pool)
    {
        $this->pool = $pool;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPool()
    {
        return $this->pool;
    }


    public function __toArray() {
        return array_merge(parent::__toArray(), [
            'pool' => $this->pool,
            'garage' => $this->garage
        ]);
    }
}
