<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass="BookingRepository")
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $expiration_date;

    /**
     * @ORM\Column(type="integer")
     */
    private $room;


    /**
     * @var Tenant
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant", inversedBy="bookings")
     */
    private $tenant;


    /**
     * @var BookingObject
     * @ORM\ManyToOne(targetEntity="App\Entity\BookingObject", inversedBy="bookings")
     */
    private $bookingObject;

    public function __construct()
    {
        try {
            $date = new \DateTime();
            $date->add(new \DateInterval('P7D'));
        } catch (\Exception $e) {
            $date = new \DateTime();
        }
        $this->expiration_date = $date;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getExpirationDate()
    {
        return date_format($this->expiration_date, 'Y-m-d');
    }

    public function getRoom(): ?int
    {
        return $this->room;
    }

    public function setRoom(int $room): self
    {
        $this->room = $room;

        return $this;
    }

    /**
     * @param Tenant $tenant
     * @return Booking
     */
    public function setTenant(Tenant $tenant): Booking
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return Tenant
     */
    public function getTenant(): Tenant
    {
        return $this->tenant;
    }

    /**
     * @param BookingObject $bookingObject
     * @return Booking
     */
    public function setBookingObject(BookingObject $bookingObject): Booking
    {
        $this->bookingObject = $bookingObject;
        return $this;
    }

    /**
     * @return BookingObject
     */
    public function getBookingObject(): BookingObject
    {
        return $this->bookingObject;
    }
}
