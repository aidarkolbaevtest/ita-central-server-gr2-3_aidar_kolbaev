<?php

namespace App\Model\BookingObject;

use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Repository\LandlordRepository;

class BookingObjectHandler
{
    /**
     * @var LandlordRepository
     */
    private $landlordRepository;


    public function __construct(LandlordRepository $landlordRepository)
    {
        $this->landlordRepository = $landlordRepository;
    }

    /**
     * @param BookingObject $bookingObject
     * @param array $data
     * @return BookingObject
     */
    public function createNewAbstractBookingObject(BookingObject $bookingObject, array $data) {
        $bookingObject->setName($data['name']);
        $bookingObject->setAddress($data['address']);
        $bookingObject->setContactNumber($data['contact_number']);
        $bookingObject->setNumberOfRooms($data['number_of_rooms']);
        $bookingObject->setPricePerNight($data['price']);
        $bookingObject->setLandlord($this->landlordRepository->findOneByEmail($data['email']));

        return $bookingObject;
    }



    public function createNewBookingObject(array $data) {
        if ($data['type'] === 'pension') {
            return $this->createNewPension($data);
        } elseif ($data['type'] === 'cottage') {
            return $this->createNewCottage($data);
        } else {
            return $this->createNewAbstractBookingObject(new BookingObject(), $data);
        }
    }


    public function createNewPension(array $data) {
        /** @var Pension $pension */
        $pension = $this->createNewAbstractBookingObject(new Pension(), $data);
        $pension->setBeach($data['beach']);
        $pension->setDistanceToCoast($data['distance_to_coast']);
        $pension->setType('pension');
        return $pension;
    }


    public function createNewCottage(array $data) {
        /** @var Cottage $cottage */
        $cottage = $this->createNewAbstractBookingObject(new Cottage(), $data);
        $cottage->setGarage($data['garage']);
        $cottage->setPool($data['pool']);
        $cottage->setType('cottage');
        return $cottage;
    }


//    public function updateBookingObject(BookingObject $bookingObject, array $data)
//    {
//        if ($data['price'] !== null) {
//            $bookingObject->setPricePerNight($data['price']);
//        }
//        if ($data['number_of_rooms'] !== null) {
//            $bookingObject->setNumberOfRooms($data['number_of_rooms']);
//        }
//        if ($data['contact_number'] !== null) {
//            $bookingObject->setContactNumber($data['contact_number']);
//        }
//        if ($data['address'] !== null) {
//            $bookingObject->setAddress($data['address']);
//        }
//    }
}
