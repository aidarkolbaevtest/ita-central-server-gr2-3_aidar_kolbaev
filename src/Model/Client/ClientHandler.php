<?php

namespace App\Model\Client;

use App\Entity\Client;
use App\Entity\Landlord;
use App\Entity\Tenant;

class ClientHandler
{
    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_FACEBOOK = "facebook";
    const SOC_NETWORK_GOOGLE = "google";

    /**
     * @param Client $client
     * @param array $data
     * @return Client
     */
    public function createNewAbstractClient(Client $client, array $data) {
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $password = $this->encodePlainPassword($data['password']);
        $client->setPassword($password);
        $client->setVkId($data['vkId'] ?? null);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);

        return $client;
    }


    /**
     * @param array $data
     * @return Client
     */
    public function createNewClient(array $data) {
        if (in_array('ROLE_TENANT', $data['roles'])) {
            return $this->createNewTenant($data);
        } elseif (in_array('ROLE_LANDLORD', $data['roles'])) {
            return $this->createNewLandlord($data);
        } else {
            return $this->createNewAbstractClient(new Client(), $data);
        }
    }


    /**
     * @param array $data
     * @return Tenant
     */
    public function createNewTenant(array $data) {
        /** @var Tenant $tenant */
        $tenant = $this->createNewAbstractClient(new Tenant(), $data);
        $tenant->setRoles(['ROLE_TENANT']);
        return $tenant;
    }


    /**
     * @param array $data
     * @return Landlord
     */
    public function createNewLandlord(array $data) {
        /** @var Landlord $landlord */
        $landlord = $this->createNewAbstractClient(new Landlord(), $data);
        $landlord->setRoles(['ROLE_LANDLORD']);
        return $landlord;
    }

    /**
     * @param string $plainPassword
     * @return string
     */
    public function encodePlainPassword(string $plainPassword) {
        return md5($plainPassword) . md5($plainPassword . '2');
    }


    public function updateClient(Client $client, array $data)
    {
        if ($data['email'] !== null) {
            $client->setEmail($data['email']);
        }
        if ($data['passport'] !== null) {
            $client->setPassport($data['passport']);
        }
        if ($data['password'] !== null) {
            $password = $this->encodePlainPassword($data['password']);
            $client->setPassword($password);
        }
        if ($data['network'] !== null) {
            foreach ($data['network'] as $network => $uid) {
                $client->setSocialId($network, $uid);
            }
        }
    }
}
