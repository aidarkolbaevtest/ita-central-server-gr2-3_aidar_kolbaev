<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\User;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ClientFixtures extends Fixture
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
        $client = $this->clientHandler->createNewTenant([
            'email' => '123@123.ru',
            'passport' => 'some & passport',
            'password' => 'pass_1234',
            'call_name' => 'Dyadya Vasya'
        ]);

        $manager->persist($client);

        $client = $this->clientHandler->createNewLandlord([
            'email' => '321@321.ru',
            'passport' => 'passport & some',
            'password' => 'pass_4321',
            'full_name' => 'Vasya Dyadya'
        ]);

        $manager->persist($client);
        $manager->flush();
    }
}
