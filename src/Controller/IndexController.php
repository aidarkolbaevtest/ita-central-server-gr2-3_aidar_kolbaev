<?php

namespace App\Controller;

use App\Entity\BookingObject;
use App\Entity\Client;
use App\Entity\Booking;
use App\Entity\Tenant;
use App\Model\BookingObject\BookingObjectHandler;
use App\Model\Client\ClientHandler;
use App\Repository\BookingObjectRepository;
use App\Repository\ClientRepository;
use App\Repository\BookingRepository;
use App\Repository\TenantRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityRepository;

class IndexController extends Controller
{
    /**
     * @Route("/ping", name="app_index")
     */
    public function pingAction()
    {
        return new JsonResponse(['result' => 'pong']);
    }

    /**
     * @Route("/client/password/encode", name="app_client_password_encode")
     * @param ClientHandler $clientHandler
     * @param Request $request
     * @return JsonResponse
     */
    public function passwordEncodeAction(
        ClientHandler $clientHandler,
        Request $request
    )
    {
        return new JsonResponse(
            [
                'result' => $clientHandler->encodePlainPassword(
                    $request->query->get('plainPassword')
                )
            ]
        );
    }


    /**
     * @Route("/client/email/{email}", name="app_patch_client_by_email")
     * @Method("PATCH")
     * @param string $email
     * @param ObjectManager $manager
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param Request $request
     * @return JsonResponse
     */
    public function patchClientByEmailAction(
        string $email,
        ObjectManager $manager,
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        Request $request
    ) {
        $client = $clientRepository->findOneByEmail($email);
        if ($client) {
            $clientHandler->updateClient($client, [
                'email' => $request->request->get('email', null),
                'passport' => $request->request->get('passport', null),
                'password' => $request->request->get('password', null),
                'network'  => $request->request->get('network', null)
            ]);
            $manager->flush();
            return new JsonResponse($client->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client/social_network/{network}/{uid}", name="app_client_by_network")
     * @param string $network
     * @param string $uid
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @return JsonResponse
     * @Method("GET")
     */
    public function clientBySocialNetworkAction(
        string $network,
        string $uid,
        ClientRepository $clientRepository,
        ClientHandler $clientHandler
    ) {
        if ($network === $clientHandler::SOC_NETWORK_VKONTAKTE) {
            $network = 'vk';
        }
        $client = $clientRepository->getBySocialNetwork($network, $uid);
        if ($client) {
            return new JsonResponse($client->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportOrEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/check_client_credentials/{encodedPassword}/{email}", name="app_check_client_credentials")
     * @Method("HEAD")
     * @param string $encodedPassword
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function checkClientCredentialsAction(
        string $encodedPassword,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPasswordAndEmail($encodedPassword, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');
        $data['vkId'] = $request->request->get('vkId');
        $data['faceBookId'] = $request->request->get('faceBookId');
        $data['googleId'] = $request->request->get('googleId');
        $data['roles'] = $request->request->get('roles');

        if(empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: '.var_export($data,1)],406);
        }

        if ($clientRepository->findOneByPassportOrEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'],406);
        }

        $client = $clientHandler->createNewClient($data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }


    /**
     * @Route("/booking_object", name="app_create_booking_object")
     * @Method("POST")
     * @param BookingObjectHandler $bookingObjectHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     */
    public function createBookingObjectAction(
        BookingObjectHandler $bookingObjectHandler,
        ObjectManager $manager,
        Request $request,
        BookingObjectRepository $bookingObjectRepository
    ) {
        $data['type'] = $request->request->get('type');
        $data['name'] = $request->request->get('name');
        $data['number_of_rooms'] = $request->request->get('number_of_rooms');
        $data['contact_number'] = $request->request->get('contact_number');
        $data['address'] = $request->request->get('address');
        $data['price'] = $request->request->get('price');
        $data['email'] = $request->request->get('email');
        $data['garage'] = $request->request->get('garage');
        $data['pool'] = $request->request->get('pool');
        $data['beach'] = $request->request->get('beach');
        $data['distance_to_coast'] = $request->request->get('distance_to_coast');

        if(
            empty($data['email']) ||
            empty($data['type']) ||
            empty($data['name']) ||
            empty($data['number_of_rooms']) ||
            empty($data['contact_number']) ||
            empty($data['address']) ||
            empty($data['price'])
        ) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: '.var_export($data,1)],406);
        }


        if ($bookingObjectRepository->findOneByNameOrContactNumber($data['name'], $data['contact_number'])) {
            return new JsonResponse(['error' => 'Объект бронирования уже существует'],406);
        }

        $bookingObject = $bookingObjectHandler->createNewBookingObject($data);

        $manager->persist($bookingObject);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }


    /**
     * @Route("/booking_object/{name}/{contact_number}")
     * @Method("HEAD")
     * @param string $name
     * @param string $contact_number
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     */
    public function bookingObjectExistsAction(
        string $name,
        string $contact_number,
        BookingObjectRepository $bookingObjectRepository)
    {
        if ($bookingObjectRepository->findOneByNameOrContactNumber($name, $contact_number)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }


    /**
     * @Route("/booking_object", name="app-booking-objects-get")
     * @Method("GET")
     * @param Request $request
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     */
    public function bookingObjectsByParametersAction(Request $request, BookingObjectRepository $bookingObjectRepository) {
        $data['name'] = $request->query->get('name');
        $data['min_price'] = $request->query->get('min_price');
        $data['max_price'] = $request->query->get('max_price');
        $data['type'] = $request->query->get('type');
        $data['options'] = $request->query->get('options');
        if (
            empty($data['name']) &&
            empty($data['type']) &&
            empty($data['min_price']) &&
            empty($data['max_price']) &&
            empty($data['options'])
        ) {
            $bookingObjects = $bookingObjectRepository->findAll();
        } else {
            $bookingObjects = $bookingObjectRepository
            ->findByParameters($data['name'], $data['type'], $data['min_price'], $data['max_price']);
        }
        $array = [];
        foreach ($bookingObjects as $bookingObject) {
            $array[] = $bookingObject->__toArray();
        }
        return new JsonResponse($array);
    }


    /**
     * @Route("/booking_object/{id}", requirements={"id" : "\d+"}, name="app-booking-object-by-id")
     * @Method("GET")
     * @param int $id
     * @param BookingObjectRepository $bookingObjectRepository
     * @param BookingRepository $bookingRepository
     * @return JsonResponse
     */
    public function bookingObjectByIdAction(
        int $id,
        BookingObjectRepository $bookingObjectRepository,
        BookingRepository $bookingRepository
    ) {
        $bookingObject = $bookingObjectRepository->find($id);
        $busy_rooms = $bookingRepository->getBookingsByBookingObjectId($id);
        $rooms = [];
        foreach ($busy_rooms as $room) {
            $rooms['busy_rooms'][] = $room;
        }
        if ($bookingObject) {
            return new JsonResponse(array_merge($bookingObject->__toArray(), $rooms));
        } else {
            throw new NotFoundHttpException();
        }
    }


    /**
     * @Route("/booking")
     * @Method("POST")
     * @param Request $request
     * @param BookingObjectRepository $bookingObjectRepository
     * @param ObjectManager $manager
     * @param TenantRepository $tenantRepository
     * @return JsonResponse
     */
    public function bookingAction(
        Request $request,
        BookingObjectRepository $bookingObjectRepository,
        ObjectManager $manager,
        TenantRepository $tenantRepository
    ) {
        $bookingObjectId = $request->request->get('object');
        $tenantEmail = $request->request->get('tenant');
        $room = $request->request->get('room');
        $booking = new Booking();
        $bookingObject = $bookingObjectRepository->find($bookingObjectId);
        $booking->setBookingObject($bookingObject);
        $booking->setRoom($room);
        $tenant = $tenantRepository->findOneByEmail($tenantEmail);
        $booking->setTenant($tenant);
        $manager->persist($booking);
        $manager->flush();
        return new JsonResponse(['result' => 'ok']);
    }


    /**
     * @Route("/booking/{email}")
     * @Method("HEAD")
     * @param string $email
     * @param BookingRepository $bookingRepository
     * @param TenantRepository $tenantRepository
     * @return JsonResponse
     */
    public function bookingCheckAction(string $email, BookingRepository $bookingRepository, TenantRepository $tenantRepository) {
        $tenant = $tenantRepository->findOneByEmail($email);
        if ($bookingRepository->findByTenantId($tenant)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client/email/{email}", name="app_client_by_email")
     * @Method("GET")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientByEmailAction(
        string $email,
        ClientRepository $clientRepository)
    {
        $client = $clientRepository->findOneByEmail($email);
        if ($client) {
            return new JsonResponse($client->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }
}
